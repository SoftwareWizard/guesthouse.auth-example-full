import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/@core/auth/auth-guard.service';
import { RoleGuard } from '@app/@core/auth/role-guard.service';
import { marker } from '@biesbjerg/ngx-translate-extract-marker';

import { AuthGuardWithForcedLogin } from '../../@core/auth/auth-guard-with-forced-login.service';
import { DetailComponent } from './detail/detail.component';
import { EmployeeComponent } from './employee.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeeComponent,
    canActivate: [AuthGuardWithForcedLogin, RoleGuard],
    data: { title: marker('Employee'), role: 'Employee' },
  },
  {
    path: 'detail',
    component: DetailComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: { title: marker('Employee Detail'), role: 'Employee' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmployeeRoutingModule {}
